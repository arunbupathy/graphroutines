CXX = g++ -Wall
CC = gcc -Wall
OPFLAGS = -O2 -march=native
LDFLAGS = -lm
PREFIX = /usr/local

all: graphroutines.o
	ar rcs libgraphroutines.a graphroutines.o

example: example.o graphroutines.o
	$(CXX) $? $(LDFLAGS) -o $@

example.o: example.cpp
	$(CXX) $(OPFLAGS) $? -c

graphroutines.o: graphroutines.c
	$(CC) $(OPFLAGS) $? -c

install: all
	install libgraphroutines.a $(PREFIX)/lib
	install -m 0644 graphroutines.h $(PREFIX)/include

clean:
	rm -f graphroutines.o example.o libgraphroutines.a example
