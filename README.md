# graphroutines

### A repository of codes for doing various operations on connectivity graphs / neighbour lists.

Presently, the project contains codes for performing the following:

- Creating a graph structure (full-graph), modifying it and demarcating a sub-portion of it (the sub-graph). There can be only one contiguous full-graph, and one sub-graph.

- Given a sub-graph, find its pivot points. Pivot points are vertices whose removal will break the sub-graph into disconnected parts. The standard terminology for pivots is either *articulation points* or *cut-vertices*.

- Given a sub-graph, find all vertices that are adjacent to it (in the full-graph). Adjacent vertices are those that can be added to the sub-graph without creating multiple disconnected parts.

- Functions to grow / shrink the sub-graph one vertex at a time. All edges connecting the respective vertex to the sub-graph will be added / removed together. If the proposed vertex addition is not adjacent to the sub-graph, it will not be added. If the proposed vertex removal would break the graph, it will not be removed.

The graph objects, a set of four C-structs, are specific to these set of codes, and have been defined in the header file. They allow simultaneous demarcation of a sub portion (sub-graph) of the original graph, by flagging the corresponding edges. It is an undirected graph, meaning the forward and reverse edges between two vertices are the same. Also, there cannot be more than one edge between any two vertices.

The routines *do not* allow growing / shrinking the sub-graph by the addition / removal of edges. I may never implement this feature, as I do not require it. Feature submissions are welcome as long as you do not touch the existing APIs and graph objects.

The codes are in C, and a makefile is provided for compiling the codes. An example program showing the basic usage is also provided.
