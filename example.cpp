
/* This program provides an example of how to use the graphroutines
 * to build a connectivity graph and perform various operations on it.
 * 
 * Consider the connectivity graph shown below:
 * 
 *  (0) + + + + (1) + + + + (2)         (3)
 *   +           +           +           |
 *   +           +           +           |
 *   +           +           +           |
 *   +           +           +           |
 *  (4)         (5) + + + + (6) - - - - (7)
 *   |           +           +           |
 *   |           +           +           |
 *   |           +           +           |
 *   |           +           +           |
 *  (8) - - - - (9)+ + + + (10)- - - - (11)
 *   |                       |
 *   |                       |
 *   |                       |
 *   |                       |
 *  (12)        (13)- - - - (14)- - - - (15)
 *                           |           |
 *                           |           |
 *                           |           |
 *                           |           |
 *                          (16)- - - - (17)
 * 
 * The numbers denote the the vertices which are connected
 * with dashes or pluses (edges). This is our full graph.
 * The edges marked by pluses form a connected sub-graph.
 * 
 * Let us see how to:
 * 0) Create the full graph structure, and how to define the sub-graph
 * 1) Grow the sub-graph by adding vertices one at a time
 * 2) Remove a vertex from the sub-graph without breaking it
 * 3) Check if a vertex is a pivot, if it is in the sub-graph
 * 4) Check if a vertex is adjacent, if it is outside the sub-graph
 * 
 * Author: Arunkumar Bupathy (arunbupathy@gmail.com)
 */

////////////////////////////////////////////////////////////////////////////

# include <iostream>
# include <cstdlib>
# include <cmath>

# include "graphroutines.h"

////////////////////////////////////////////////////////////////////////////

/* Beginning of main program */

int main(int argc, char * argv[])
{
    int N = 18; // number of vertices in our graph

    // this is the full graph object
    full_graph * graph = alloc_full_graph(N);
    
    /* Let us now add all the edges of the main graph */
    /* NOTE: It is the user's responsibility to define a properly connected full graph! */
    std::cout << std::endl << "Creating the full graph by adding the appropriate edges... ";
    add_edge_full(graph, 0, 1);
    add_edge_full(graph, 0, 4);
    add_edge_full(graph, 1, 2);
    add_edge_full(graph, 1, 5);
    add_edge_full(graph, 2, 6);
    add_edge_full(graph, 5, 6);
    add_edge_full(graph, 5, 9);
    add_edge_full(graph, 6, 7);
    add_edge_full(graph, 6, 10);
    add_edge_full(graph, 7, 3);
    add_edge_full(graph, 7, 11);
    add_edge_full(graph, 10, 11);
    add_edge_full(graph, 9, 10);
    add_edge_full(graph, 4, 8);
    add_edge_full(graph, 8, 9);
    add_edge_full(graph, 8, 12);
    add_edge_full(graph, 14, 15);
    add_edge_full(graph, 14, 16);
    add_edge_full(graph, 15, 17);
    add_edge_full(graph, 16, 17);
    add_edge_full(graph, 14, 13);
    
    /* Let us query the properties of the full-graph */
    std::cout << "The full graph has " << graph->num_vert_full << " vertices and ";
    std::cout << graph->num_edge_full << " edges." << std::endl;
    /* The output should be:
       The full graph has 18 vertices and 21 edges. */
    
    /* Let us now try to create the sub-graph by adding some vertices */
    std::cout << std::endl << "Let us try adding vertex 2 to the sub-graph." << std::endl;
    add_vertex(graph, 2);
    /* The vertex is not added, because the graph construction is invalid */
    /* The graph construction is invalid because the edge 10--14 is not added */
    /* So the full-graph is made of two disconnected islands of graphs. */
    /* You can alternatively use check_graph_integrity(graph) to check for the same */
    
    /* We deliberately did not add this edge, so let's add it now */
    std::cout << std::endl << "Adding the edge 10-14 to the full-graph to fix the broken graph... ";
    add_edge_full(graph, 10, 14);
    /* That completes the definition of the full graph */
    
    /* Let us query the properties of the full-graph again */
    std::cout << "The full graph has now " << graph->num_vert_full << " vertices and ";
    std::cout << graph->num_edge_full << " edges." << std::endl << std::endl;
    /* The output should be:
       The full graph has 18 vertices and 22 edges. */
    
    /* Let us now create the sub-graph by adding the corresponding the vertices */
    std::cout << "Creating the sub-graph by adding vertices in the order 0, 1, 2, 10, 6, 4, 9, 5." << std::endl;
    add_vertex(graph, 0);
    add_vertex(graph, 1);
    add_vertex(graph, 2);
    add_vertex(graph, 10);
    add_vertex(graph, 6);
    add_vertex(graph, 4);
    add_vertex(graph, 9);
    add_vertex(graph, 5);
    /* Note that the vertices 9 and 10 weren't added, because they were not adjacent at the time of addition */
    
    /* Let us query the properties of the sub-graph */
    query_graph_props(graph);
    /* The output should be self-explanatory */
    
    /* Now that we have added the other vertices, 9 and 10 are adjacent so we can add them */
    std::cout << "Adding vertices 9 and 10, which could not be added earlier as they were not adjacent then." << std::endl;
    add_vertex(graph, 9);
    add_vertex(graph, 10);
    /* This completes the creation of the sub-graph shown in the illustration above */
    
    /* Let us again query the properties of the sub-graph */
    query_graph_props(graph);
    
    /* Let us remove the vertices one by one, in some random order */
    std::cout << "Destroying the sub-graph by removing vertices in the order 1, 5, 2, 9, 0, 6, 4, 10." << std::endl;
    remove_vertex(graph, 1);
    remove_vertex(graph, 5);
    remove_vertex(graph, 2);
    remove_vertex(graph, 9);
    remove_vertex(graph, 0);
    remove_vertex(graph, 6);
    remove_vertex(graph, 10);
    remove_vertex(graph, 4);
    /* As you would figure out from the output, some vertices could not be removed as the were pivots */
    
    /* Let us again query the properties of the sub-graph */
    query_graph_props(graph);
    
    /* So far so good... Let's go ahead and remove the remaining vertices too. */
    std::cout << "Removing vertices 0, 1, 2 and 6, which could not be removed earlier as they were pivots then." << std::endl;
    remove_vertex(graph, 0);
    remove_vertex(graph, 1);
    remove_vertex(graph, 2);
    remove_vertex(graph, 6);
    
    /* Let us again query the properties of the sub-graph */
    query_graph_props(graph);
    
    free_full_graph(graph);
    
    return 0;
}

/* End of main program */

////////////////////////////////////////////////////////////////////////////
