
/*  This is a set of codes that perform certain operations on
 *  connectivity graphs / neighbour lists. 
 * 
 *  Copyright (C) 2020  Arunkumar Bupathy (arunbupathy@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

# ifndef ARUN_GRAPHROUTINES
# define ARUN_GRAPHROUTINES

#include <stdbool.h>

// # define NO_CHECKS

# define MAX_NEIGH 4

/* To represent an undirected graph we need to describe the connectivity
 * of each vertex with others in the graph. We do this by declaring an array
 * of structs, i.e., each vertex gets its own struct that describes its neighborhood.
 * We also define another secondary struct for marking the sub-graph and its properties. */

typedef struct {
  int neigh[MAX_NEIGH]; // an array to hold the neighbors of the given vertex
  int max_size; // number of edges of the given vertex in the full graph
} neighbor_list;

typedef struct {
  bool ingraph; // does the vertex itself belong in the sub-graph? (i.e., ingraph==true)
  bool isadjacent; // is this vertex adjacent to the sub-graph? only makes sense if ingraph==false
  bool ispivot; // is this vertex a pivot? only makes sense if ingraph==true
} sub_graph;

/* Apart from the connectivity graph itself, we define another struct to 
 * hold some properties of the entire graph. This is a collective property
 * of the graph, so only one such struct needs to be declared per graph. */

typedef struct {
    int num_vert; // number of vertices in the sub-graph
    int num_edge; // number of edges in the sub-graph
    int num_adj; // number of vertices adjacent to the sub-graph
    int num_nonpiv; // number of vertices in the sub-graph that are not pivots
} graph_properties;

/* This struct holds the list of adjacent and non-pivot vertices of the sub-graph
 * at any given point of time and is updated automatically */

typedef struct {
    int * adjacents; // pointer to an array that will hold the adjacent vertices
    int * nonpivots; // pointer to an array that will hold the nonpivot vertices
} vertex_lists;

/* Finally, we define a convenient wrapper structure around the graph objects to make life easier */

typedef struct {
    neighbor_list * nlist;
    sub_graph * subgr;
    vertex_lists * vlists;
    graph_properties * gprops;
    int num_vert_max; // this is the requested size (number of vertices) of the graph
    int num_vert_full; // size of the full graph after the edges have been added
    int num_edge_full; // number of edges of the full graph
    bool isvalid;
} full_graph;

#ifdef __cplusplus
extern "C" {
#endif
    
full_graph * alloc_full_graph(int);

void free_full_graph(full_graph *);

void add_edge_full(full_graph *, int, int);
    
void remove_edge_full(full_graph *, int, int);

int check_graph_integrity(full_graph *);

void get_all_pivots(full_graph *);

void get_adjacent(full_graph *);

int remove_vertex(full_graph *, int);

int add_vertex(full_graph *, int);

void query_graph_props(full_graph *);

#ifdef __cplusplus
}
#endif

# endif
