
/*  This is a set of codes that perform certain operations on
 *  connectivity graphs / neighbour lists. 
 * 
 *  Copyright (C) 2020  Arunkumar Bupathy (arunbupathy@gmail.com)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

# include <stdlib.h>
# include <stdio.h>
# include <stdbool.h>
# include <limits.h>
# include "graphroutines.h"

////////////////////////////////////////////////////////////////////////////

full_graph * alloc_full_graph(int N)
{
    full_graph * fg = (full_graph *) calloc(1, sizeof(full_graph));
    
    fg->nlist = (neighbor_list *) calloc(N, sizeof(neighbor_list));
    for(int i0 = 0; i0 < N; ++i0)
    {
        for(int j0 = 0; j0 < MAX_NEIGH; ++j0)
        {
            fg->nlist[i0].neigh[j0] = -1;
        }
    }
    
    fg->subgr = (sub_graph *) calloc(N, sizeof(sub_graph));
    
    fg->vlists = (vertex_lists *) calloc(1, sizeof(vertex_lists));
    fg->vlists->adjacents = (int *) calloc(N, sizeof(int));
    fg->vlists->nonpivots = (int *) calloc(N, sizeof(int));
    
    fg->gprops = (graph_properties *) calloc(1, sizeof(graph_properties));
    
    fg->num_vert_max = N;
    
    return fg;
}

////////////////////////////////////////////////////////////////////////////

void free_full_graph(full_graph * fg)
{
    free(fg->gprops);
    
    free(fg->vlists->adjacents);
    free(fg->vlists->nonpivots);
    free(fg->vlists);
    
    free(fg->subgr);
    
    free(fg->nlist);
    
    free(fg);
    
    return;
}

////////////////////////////////////////////////////////////////////////////

void add_edge_full(full_graph * fg, int v1, int v2)
{
    int i0 = fg->nlist[v1].max_size;
    int i1 = 0;
    while(fg->nlist[v1].neigh[i1] != v2 && i1 < (i0-1))
        i1++;
    if(fg->nlist[v1].neigh[i1] != v2)
    {
        fg->nlist[v1].neigh[i0] = v2;
        fg->nlist[v1].max_size = i0 + 1;
        if(i0 == 0)
            fg->num_vert_full++;
    }
    
    int j0 = fg->nlist[v2].max_size;
    int j1 = 0;
    while(fg->nlist[v2].neigh[j1] != v1 && j1 < (j0-1))
        j1++;
    if(fg->nlist[v2].neigh[j1] != v1)
    {
        fg->nlist[v2].neigh[j0] = v1;
        fg->nlist[v2].max_size = j0 + 1;
        if(j0 == 0)
            fg->num_vert_full++;
        
        fg->num_edge_full++;
    }
    
    return;
}

////////////////////////////////////////////////////////////////////////////

void remove_edge_full(full_graph * fg, int v1, int v2)
{
    if(fg->nlist[v1].max_size == 0 || fg->nlist[v2].max_size == 0)
        return;
    
    int i0 = fg->nlist[v1].max_size - 1;
    int i1 = 0;
    while(fg->nlist[v1].neigh[i1] != v2 && i1 < i0)
        i1++;
    if(fg->nlist[v1].neigh[i1] == v2)
    {
        fg->nlist[v1].neigh[i1] = fg->nlist[v1].neigh[i0];
        fg->nlist[v1].neigh[i0] = -1;
        fg->nlist[v1].max_size = i0;
        if(i0 == 0)
            fg->num_vert_full--;
    }
    
    int j0 = fg->nlist[v2].max_size - 1;
    int j1 = 0;
    while(fg->nlist[v2].neigh[j1] != v1 && j1 < j0)
        j1++;
    if(fg->nlist[v2].neigh[j1] == v1)
    {
        fg->nlist[v2].neigh[j1] = fg->nlist[v2].neigh[j0];
        fg->nlist[v2].neigh[j0] = -1;
        fg->nlist[v2].max_size = j0;
        if(j0 == 0)
            fg->num_vert_full--;
    
        fg->num_edge_full--;
    }
    
    return;
}

////////////////////////////////////////////////////////////////////////////

/* This function is not for public use */
void dfs_visit_full(full_graph * fg, bool * visited, int * nvisited, int current)
{
    visited[current] = true;
    *nvisited = *nvisited + 1;
    
    for(int i0 = 0; i0 < fg->nlist[current].max_size; ++i0)
    {
        int neighbor = fg->nlist[current].neigh[i0];
        if(!visited[neighbor])
            dfs_visit_full(fg, visited, nvisited, neighbor);
    }
    
    return;
}

////////////////////////////////////////////////////////////////////////////

int check_graph_integrity(full_graph * fg)
{
    int N = fg->num_vert_max;
    for(int i0 = 0; i0 < N; ++i0)
    {
        if(fg->nlist[i0].max_size == 0)
        {
            fprintf(stderr, "ERROR: There is at least one isolated vertex that is not part of the full-graph.\n");
            return -1;
        }
    }
    
    int nvisited = 0;
    bool * visited = calloc(N, sizeof(bool));
    dfs_visit_full(fg, visited, &nvisited, 0);
    free(visited);
    
    if(nvisited != N)
    {
        fprintf(stderr, "ERROR: The graph construction is not valid. There are multiple disconnected islands of vertices.\n");
        return -1;
    }
    
    fg->isvalid = true;
    return 0;
}

////////////////////////////////////////////////////////////////////////////

/* This function is not for public use */
void dfs_get_pivots(full_graph * fg, int * discovery, int * earliest, bool * visited, int * parent, int v1, int * time)
{
    visited[v1] = true;
    discovery[v1] = *time + 1;
    earliest[v1] = *time + 1;
    int children = 0;
    
    for(int i0 = 0; i0 < fg->nlist[v1].max_size; ++i0)
    {
        int v2 = fg->nlist[v1].neigh[i0];
        
        if(fg->subgr[v2].ingraph)
        {
            if(!visited[v2])
            {
                children++;
                parent[v2] = v1;
                *time += 1;
                
                dfs_get_pivots(fg, discovery, earliest, visited, parent, v2, time);
                
                earliest[v1] = earliest[v1] < earliest[v2] ? earliest[v1] : earliest[v2];
                
                if(parent[v1] == -1 && children > 1)
                {
                    fg->subgr[v1].ispivot = true;
                }
                
                if(parent[v1] != -1 && earliest[v2] >= discovery[v1])
                {
                    fg->subgr[v1].ispivot = true;
                }
            }
            else if(parent[v1] != v2)
            {
                earliest[v1] = earliest[v1] < discovery[v2] ? earliest[v1] : discovery[v2];
            }
        }
    }
    
    return;
}

////////////////////////////////////////////////////////////////////////////

void get_all_pivots(full_graph * fg)
{
    # ifndef NO_CHECKS
    if(!fg->isvalid)
    {
        check_graph_integrity(fg);
        if(!fg->isvalid)
            return;
    }
    # endif
    
    int num_vert_full = fg->num_vert_full;
    
    for(int i0 = 0; i0 < num_vert_full; ++i0)
        fg->subgr[i0].ispivot = false;
    
    if(fg->gprops->num_vert > 2)
    {
        int discovery[num_vert_full];
        int earliest[num_vert_full];
        int parent[num_vert_full];
        bool visited[num_vert_full];
        
        for(int i0 = 0; i0 < num_vert_full; ++i0)
        {
            discovery[i0] = 0;
            earliest[i0] = INT_MAX;
            visited[i0] = false;
            parent[i0] = -1;
        }
        
        int time = 0;
        int v1 = 0;
        while(!fg->subgr[v1].ingraph)
            v1++;
        dfs_get_pivots(fg, discovery, earliest, visited, parent, v1, &time);
    }
    
    fg->gprops->num_nonpiv = 0;
    for(int i0 = 0; i0 < num_vert_full; ++i0)
    {
        if(fg->subgr[i0].ingraph && !fg->subgr[i0].ispivot)
            fg->vlists->nonpivots[fg->gprops->num_nonpiv++] = i0;
    }
    
    return;
}

////////////////////////////////////////////////////////////////////////////

/* We don't ever use this function, but it's there just in case someone needs it */
void get_adjacent(full_graph * fg)
{
    # ifndef NO_CHECKS
    if(!fg->isvalid)
    {
        check_graph_integrity(fg);
        if(!fg->isvalid)
            return;
    }
    # endif
    
    fg->gprops->num_adj = 0;
    
    for(int v1 = 0; v1 < fg->num_vert_full; ++v1) // take vertices not part of the sub-graph
    {
        fg->subgr[v1].isadjacent = false;
        
        if(fg->subgr[v1].ingraph) continue;
        // make sure v1 is not already part of sub-graph
        
        bool ingraph = false; // is at least one of the neighbours of v1 in the sub-graph?
        for(int i0 = 0; i0 < fg->nlist[v1].max_size; ++i0)
        {
            int v2 = fg->nlist[v1].neigh[i0];
            ingraph = ingraph || fg->subgr[v2].ingraph;
        }
        
        if(ingraph) // if so, v1 is an adjacent vertex
        {
            fg->subgr[v1].isadjacent = true;
            fg->vlists->adjacents[fg->gprops->num_adj++] = v1;
        }
    }
    
    return;
}

////////////////////////////////////////////////////////////////////////////

/* This function is not for public use */
void get_adjacent_rem(full_graph * fg, int v1)
{
    // this preprocessor directive is only for internal use; don't touch!
    # ifndef NO_LAST_REMOVE
    if(fg->gprops->num_vert > 0)
    # endif
        fg->subgr[v1].isadjacent = true;
    
    for(int i0 = 0; i0 < fg->nlist[v1].max_size; ++i0)
    {
        int v2 = fg->nlist[v1].neigh[i0];
        
        if(fg->subgr[v2].ingraph) continue;
        // make sure v2 is not already part of sub-graph
        
        bool ingraph = false; // is at least one of the neighbours of v2 in the sub-graph?
        for(int i0 = 0; i0 < fg->nlist[v2].max_size; ++i0)
        {
            int v3 = fg->nlist[v2].neigh[i0];
            ingraph = ingraph || fg->subgr[v3].ingraph;
        }
        
        // if none of v2's neighbors are in the sub-graph, then v2 cannot be adjacent
        if(!ingraph)
            fg->subgr[v2].isadjacent = false;
    }
    
    fg->gprops->num_adj = 0;
    for(int i0 = 0; i0 < fg->num_vert_full; ++i0)
    {
        if(fg->subgr[i0].isadjacent)
            fg->vlists->adjacents[fg->gprops->num_adj++] = i0;
    }
    
    return;
}

////////////////////////////////////////////////////////////////////////////

/* This function is not for public use */
void get_adjacent_add(full_graph * fg, int v1)
{
    fg->subgr[v1].isadjacent = false;
    // remove v1 from the adjacent list coz we just added it to the sub-graph
    
    for(int i0 = 0; i0 < fg->nlist[v1].max_size; ++i0)
    {
        int v2 = fg->nlist[v1].neigh[i0];
        
        if(fg->subgr[v2].ingraph) continue;
        // make sure v2 is not already part of sub-graph
        
        fg->subgr[v2].isadjacent = true;
    }
    
    fg->gprops->num_adj = 0;
    for(int i0 = 0; i0 < fg->num_vert_full; ++i0)
    {
        if(fg->subgr[i0].isadjacent)
            fg->vlists->adjacents[fg->gprops->num_adj++] = i0;
    }
        
    return;
}

////////////////////////////////////////////////////////////////////////////

int remove_vertex(full_graph * fg, int v1)
{
    # ifndef NO_CHECKS
    if(!fg->isvalid)
    {
        check_graph_integrity(fg);
        if(!fg->isvalid)
            return -2;
    }
    
    if(!fg->subgr[v1].ingraph)
    {
        printf("This vertex is already *not* part of the sub-graph! Nothing to do.\n");
        return 1;
    }
    if(fg->subgr[v1].ispivot)
    {
       printf("Vertex %d is a pivot point of the sub-graph and cannot be removed.\n", v1);
       return -1;
    }
    # endif
        
    fg->subgr[v1].ingraph = false;
        
    for(int i0 = 0; i0 < fg->nlist[v1].max_size; ++i0)
    {
        int v2 = fg->nlist[v1].neigh[i0];
        if(fg->subgr[v2].ingraph)
            fg->gprops->num_edge--;
    }
    
    fg->gprops->num_vert--;
    
    // get num_adj vertices
    get_adjacent_rem(fg, v1);
    
    // get pivots
    get_all_pivots(fg);
    
    return 0;
}

////////////////////////////////////////////////////////////////////////////

int add_vertex(full_graph * fg, int v1)
{
    # ifndef NO_CHECKS
    if(!fg->isvalid)
    {
        check_graph_integrity(fg);
        if(!fg->isvalid)
            return -2;
    }
    
    if(fg->subgr[v1].ingraph)
    {
        printf("This vertex is already part of the sub-graph! Nothing to do.\n");
        return 1;
    }
    if(fg->gprops->num_vert > 1)
    {
        if(!fg->subgr[v1].isadjacent)
        {
            printf("Vertex %d is not adjacent to the sub-graph and cannot be added to it.\n", v1);
            return -1;
        }
    }
    # endif
    
    fg->subgr[v1].ingraph = true;
    
    for(int i0 = 0; i0 < fg->nlist[v1].max_size; ++i0)
    {
        int v2 = fg->nlist[v1].neigh[i0];
        if(fg->subgr[v2].ingraph)
            fg->gprops->num_edge++;
    }
    
    fg->gprops->num_vert++;
        
    // get num_adj vertices
    get_adjacent_add(fg, v1);
    
    // get pivots
    get_all_pivots(fg);
    
    return 0;
}

////////////////////////////////////////////////////////////////////////////

void query_graph_props(full_graph * fg)
{
    printf("\nThe sub-graph has %d vertices and %d edges.\n", fg->gprops->num_vert, fg->gprops->num_edge);
    printf("%d vertices of this sub-graph are pivots.\n", (fg->gprops->num_vert - fg->gprops->num_nonpiv));
    printf("There are %d vertices adjacent to this sub-graph.\n\n", fg->gprops->num_adj);
    
    for(int i0 = 0; i0 < fg->num_vert_full; ++i0)
    {
        if(fg->subgr[i0].ingraph && fg->subgr[i0].ispivot)
            printf("Vertex %d is a pivot of the sub-graph.\n", i0);
        if(!fg->subgr[i0].ingraph && fg->subgr[i0].isadjacent)
            printf("Vertex %d is adjacent to the sub-graph.\n", i0);
    }
    printf("\n");
    
    return;
}

////////////////////////////////////////////////////////////////////////////
